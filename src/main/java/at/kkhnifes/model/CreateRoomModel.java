package at.kkhnifes.model;

public class CreateRoomModel {

    private String roomName;

    public CreateRoomModel() { }

    public CreateRoomModel(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}
