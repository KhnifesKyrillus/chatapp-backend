package at.kkhnifes.model;

import at.kkhnifes.repository.model.Room;
import at.kkhnifes.repository.model.User;

public class RoomMemberModel {

    private Room room;
    private User member;

    public RoomMemberModel() {
    }

    public RoomMemberModel(Room room, User member) {
        this.room = room;
        this.member = member;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public User getMember() {
        return member;
    }

    public void setMember(User member) {
        this.member = member;
    }
}
