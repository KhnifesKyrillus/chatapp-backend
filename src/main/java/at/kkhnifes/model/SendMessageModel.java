package at.kkhnifes.model;

import at.kkhnifes.repository.model.Room;

public class SendMessageModel {

    private Room room;
    private String text;

    public SendMessageModel() { }

    public SendMessageModel(Room room, String text) {
        this.room = room;
        this.text = text;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
