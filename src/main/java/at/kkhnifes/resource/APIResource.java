package at.kkhnifes.resource;

import at.kkhnifes.model.CreateRoomModel;
import at.kkhnifes.model.LoginModel;
import at.kkhnifes.model.RoomMemberModel;
import at.kkhnifes.model.SendMessageModel;
import at.kkhnifes.repository.DatabaseRepository;
import at.kkhnifes.repository.model.Message;
import at.kkhnifes.repository.model.Room;
import at.kkhnifes.repository.model.User;
import at.kkhnifes.util.HashUtil;
import at.kkhnifes.util.JWTClaim;
import at.kkhnifes.util.JWTUtil;
import io.smallrye.jwt.build.Jwt;
import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.annotation.PostConstruct;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

@Path("/api")
@RequestScoped
public class APIResource {

    @Inject
    JsonWebToken jwt;

    @Inject
    DatabaseRepository repository;

    @Inject
    ChatManager chatManager;

    @PostConstruct
    public void init() {

    }

    @POST
    @Path("login")
    @PermitAll
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(LoginModel loginModel) {

        String hashedPassword = HashUtil.hashString(loginModel.password);
        User user = repository.getUser(loginModel.userName);
        if (user != null) {
            if (hashedPassword == null || !hashedPassword.equals(user.getPasswordHash())) {
                return Response.status(401).build();
            }
        }
        // User does not exist
        else {
            try {
                user = repository.createUser(new User(loginModel.userName, hashedPassword));
            } catch (Exception e) {
                return Response.serverError().build();
            }
        }

        String jwt = Jwt.issuer("https://chatapp.com/")
                .upn("khnifes.kyrillus@gmail.com")
                .groups(new HashSet<>(Collections.singletonList("User")))
                .claim(Claims.nickname, loginModel.userName)
                .claim(JWTClaim.USER_ID.getValue(), user.getId())
                .sign();

        return Response.ok(jwt).build();
    }

    @GET
    @Path("getRooms")
    @RolesAllowed("User")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Room> getRooms() {
        return repository.getUsersRooms(JWTUtil.getLoggedInUser(repository, jwt));
    }

    @POST
    @Path("createRoom")
    @RolesAllowed("User")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createRoom(CreateRoomModel model) {
        User user = JWTUtil.getLoggedInUser(repository, jwt);
        if (user == null)
            return Response.status(401).build();
        return Response.ok(repository.createRoom(model.getRoomName(), user)).build();
    }

    @POST
    @Path("deleteRoom")
    @RolesAllowed("User")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteRoom(Room room) {
        User user = JWTUtil.getLoggedInUser(repository, jwt);
        if (user == null)
            return Response.status(401).build();
        return Response.ok(repository.deleteRoom(room.getId())).build();
    }

    @POST
    @Path("getRoomMembers")
    @RolesAllowed("User")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRoomMembers(Room room) {
        User user = JWTUtil.getLoggedInUser(repository, jwt);
        if (user == null)
            return Response.status(401).build();
        return Response.ok(repository.getRoomMembers(room)).build();
    }

    @GET
    @Path("getUsernames")
    @RolesAllowed("User")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsernames() {
        User user = JWTUtil.getLoggedInUser(repository, jwt);
        if (user == null)
            return Response.status(401).build();
        Collection<User> users = repository.getUsers();
        users.remove(user);
        return Response.ok(users.stream().map(User::getName).toArray()).build();
    }

    @POST
    @Path("removeRoomMember")
    @RolesAllowed("User")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeRoomMember(RoomMemberModel model) {
        User user = JWTUtil.getLoggedInUser(repository, jwt);
        if (user == null)
            return Response.status(401).build();
        repository.removeRoomMember(model.getRoom(), model.getMember());
        return Response.ok().build();
    }

    @POST
    @Path("addRoomMember")
    @RolesAllowed("User")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addRoomMember(RoomMemberModel model) {
        User user = JWTUtil.getLoggedInUser(repository, jwt);
        if (user == null)
            return Response.status(401).build();
        repository.addRoomMember(model.getRoom(), model.getMember());
        return Response.ok().build();
    }

    @POST
    @Path("sendMessage")
    @RolesAllowed("User")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendMessage(SendMessageModel model) {
        User user = JWTUtil.getLoggedInUser(repository, jwt);
        if (user == null)
            return Response.status(401).build();

        Room messageRoom = repository.getRoom(model.getRoom().getId());
        Message message = repository.createMessage(user, messageRoom, model.getText(), LocalDateTime.now());
        chatManager.notifyUsers(messageRoom.getName());

        return Response.ok(message).build();
    }

    @POST
    @Path("getMessages")
    @RolesAllowed("User")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMessages(Room room) {
        User user = JWTUtil.getLoggedInUser(repository, jwt);
        if (user == null)
            return Response.status(401).build();

        return Response.ok(repository.getRoomMessages(room)).build();
    }
}