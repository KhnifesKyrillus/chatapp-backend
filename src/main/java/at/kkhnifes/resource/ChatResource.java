package at.kkhnifes.resource;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/api/chat/{room}/{user}")
@ApplicationScoped
public class ChatResource {

    @Inject ChatManager chatManager;

    @OnOpen
    public void onOpen(Session session, @PathParam("room") String room, @PathParam("user") String userName) {
        chatManager.userLogin(room, userName, session);
    }

    @OnClose
    public void onClose(Session session, @PathParam("room") String room, @PathParam("user") String userName) {
        chatManager.userLogout(userName);
    }

    @OnError
    public void onError(Session session, @PathParam("room") String room, @PathParam("user") String userName,
                        Throwable throwable) {
        chatManager.userLogout(userName);
    }

    @OnMessage
    public void onMessage(String message, @PathParam("room") String room, @PathParam("user") String userName) {
    }
}
