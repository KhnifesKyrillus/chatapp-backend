package at.kkhnifes.resource;

import javax.inject.Singleton;
import javax.websocket.Session;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Singleton
public class ChatManager {

  private Map<String, Map<String, Session>> sessions = new ConcurrentHashMap<>();

    public boolean isUserActive(String room, String userName) {
        return sessions.get(room).containsKey(userName);
    }

    public void userLogin(String room, String userName, Session session) {
        boolean roomIsEmpty = !sessions.containsKey(room);
        Map<String, Session> users = !roomIsEmpty
                ? sessions.get(room) : new HashMap<>();

        users.put(userName, session);
        if (roomIsEmpty) {
            sessions.put(room, users);
        }
    }

    public void userLogout(String userName) {
        sessions.forEach((room, users) -> users.remove(userName));
    }

    public void notifyUsers(String room) {
        sessions.get(room).forEach((userName, session) -> session.getAsyncRemote().sendObject(new byte[] { 0 }));
    }
}
