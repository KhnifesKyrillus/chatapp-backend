package at.kkhnifes.util;

import at.kkhnifes.repository.DatabaseRepository;
import at.kkhnifes.repository.model.User;
import org.eclipse.microprofile.jwt.JsonWebToken;

public class JWTUtil {

    public static User getLoggedInUser(DatabaseRepository repository, JsonWebToken jwt) {
        long userId = Long.parseLong(
                jwt.claim(JWTClaim.USER_ID.getValue()).stream().findFirst().orElse("-1").toString()
        );
        return repository.getUserById(userId);
    }
}
