package at.kkhnifes.repository;

import at.kkhnifes.repository.model.Membership;
import at.kkhnifes.repository.model.Message;
import at.kkhnifes.repository.model.Room;
import at.kkhnifes.repository.model.User;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Collection;

@ApplicationScoped
public class DatabaseRepository {

    @Inject
    private EntityManager entityManager;

    @Transactional
    public Message createMessage(User user, Room room, String text, LocalDateTime timeStamp) {
        Message message = new Message(text, timeStamp, user);
        message.setRoom(room);
        entityManager.persist(message);
        return message;
    }

    @Transactional
    public User createUser(User user) {
        entityManager.persist(user);
        return user;
    }

    @Transactional
    public Room createRoom(String name, User user) {
        Room room = new Room(name);
        entityManager.persist(room);

        joinRoom(user, room);
        return room;
    }

    @Transactional
    public Membership joinRoom(User user, Room room) {
        Membership membership = new Membership();
        membership.setUser(user);
        membership.setRoom(room);
        entityManager.persist(membership);

        return membership;
    }

    @Transactional
    public Room deleteRoom(long id) {
        Room room = entityManager.find(Room.class, id);
        entityManager.remove(room);
        return room;
    }

    public Room getRoom(long roomId) {
        return entityManager.createNamedQuery("Room.findById", Room.class)
                .setParameter("id", roomId).getResultList().stream().findFirst().orElse(null);
    }

    public User getUserById(long userId) {
        return entityManager.find(User.class, userId);
    }

    public User getUser(String username) {

        return entityManager.createNamedQuery("User.findByName", User.class)
                .setParameter("name", username).getResultList().stream().findFirst().orElse(null);
    }

    @Transactional
    public void updateRoom(Room r) {
        entityManager.merge(r);
    }

    @Transactional
    public void removeRoomMember(Room r, User u) {
        Room room = entityManager.find(Room.class, r.getId());
        User user = entityManager.find(User.class, u.getId());

        Membership membership = entityManager.createNamedQuery("Membership.findByUserAndRoom", Membership.class)
                .setParameter("room", room).setParameter("user", user)
                .getResultList().stream().findFirst().orElse(null);

        entityManager.remove(membership);
    }

    @Transactional
    public void addRoomMember(Room r, User user) {
        Room room = entityManager.find(Room.class, r.getId());
        entityManager.createNamedQuery("User.findByName", User.class)
                .setParameter("name", user.getName())
                .getResultList().stream().findFirst()
                .ifPresent(u -> joinRoom(u, room));
    }

    public Collection<Room> getUsersRooms(User user) {
        return entityManager.createQuery("SELECT m.room FROM Membership m where m.user=(:userName)", Room.class)
                .setParameter("userName", user)
                .getResultList();
    }

    public Collection<Message> getRoomMessages(Room room) {
        return entityManager.createNamedQuery("Message.findByRoom", Message.class)
                .setParameter("room", room)
                .getResultList();
    }

    public Collection<User> getRoomMembers(Room room) {
        return entityManager.createQuery("SELECT m.user FROM Membership m where m.room=(:room)", User.class)
                .setParameter("room", room)
                .getResultList();
    }

    public Collection<User> getUsers() {
        return entityManager.createNamedQuery("User.findAll", User.class).getResultList();
    }
}
